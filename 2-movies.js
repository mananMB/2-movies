const favouriteMovies = {
  Matrix: {
    imdbRating: 8.3,
    actors: ["Keanu Reeves", "Carrie-Anniee"],
    oscarNominations: 2,
    genre: ["sci-fi", "adventure"],
    totalEarnings: "$680M",
  },
  FightClub: {
    imdbRating: 8.8,
    actors: ["Edward Norton", "Brad Pitt"],
    oscarNominations: 6,
    genre: ["thriller", "drama"],
    totalEarnings: "$350M",
  },
  Inception: {
    imdbRating: 8.3,
    actors: ["Tom Hardy", "Leonardo Dicaprio"],
    oscarNominations: 12,
    genre: ["sci-fi", "adventure"],
    totalEarnings: "$870M",
  },
  "The Dark Knight": {
    imdbRating: 8.9,
    actors: ["Christian Bale", "Heath Ledger"],
    oscarNominations: 12,
    genre: ["thriller"],
    totalEarnings: "$744M",
  },
  "Pulp Fiction": {
    imdbRating: 8.3,
    actors: ["Sameul L. Jackson", "Bruce Willis"],
    oscarNominations: 7,
    genre: ["drama", "crime"],
    totalEarnings: "$455M",
  },
  Titanic: {
    imdbRating: 8.3,
    actors: ["Leonardo Dicaprio", "Kate Winslet"],
    oscarNominations: 13,
    genre: ["drama"],
    totalEarnings: "$800M",
  },
};

const fixData = () => {
  return Object.entries(favouriteMovies).map((movie) => {
    movie[1].name = movie[0];
    return movie[1];
  });
};

const fixEarnings = (earningAmount) => {
  return earningAmount.replace(/[$M]/g, "");
};

const fixedData = fixData();

// NOTE: For all questions, the returned data must contain all the movie information including its name.
//
//Q1. Find all the movies with total earnings more than $500M.
const earningsMoreThan = (earning) => {
  return fixedData.filter((movie) => {
    return fixEarnings(movie.totalEarnings) > fixEarnings(earning);
  });
};

//Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
const oscarNominationsMoreThan = (nominationCount, earnings = "0") => {
  return earningsMoreThan(earnings).filter((movie) => {
    return movie.oscarNominations > nominationCount;
  });
};

//Q3. Find all movies of the actor "Leonardo Dicaprio".
const findByActor = (actorName) => {
  return fixedData.filter((movie) => {
    return movie.actors.includes(actorName);
  });
};

//Q4. Sort movies (based on IMDB rating)
//    if IMDB ratings are same, compare totalEarning as the secondary metric.
const sortBasedOnRating = () => {
  return fixedData
    .sort((movieA, movieB) => {
      if (movieA.imdbRating === movieB.imdbRating) {
        return (
          fixEarnings(movieA.totalEarnings) - fixEarnings(movieB.totalEarnings)
        );
      }
      return movieA.imdbRating - movieB.imdbRating;
    })
    .reverse();
};

//Q5. Group movies based on genre. Priority of genres in case of multiple genres present are:
//    drama > sci-fi > adventure > thriller > crime

const groupBasedOnGenre = (genreOrderList) => {
  let data = [...fixedData];
  return Object.fromEntries(
    genreOrderList.map((genre) => {
      return [
        genre,
        data.filter((movie) => {
          if (movie.genre.includes(genre)) {
            data = data.filter((movieInData) => {
              return movieInData.name !== movie.name;
            });
            return true;
          }
          return false;
        }),
      ];
    })
  );
};
